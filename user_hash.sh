#!/usr/bin/env bash
# Script to take the output of the above and create an MD5 sum from it, 
# placing these values in /var/log/current_users. 

# This script was tested and run on an OpenSUSE Leap 15.5 container 
# on RancherOS using GNU bash, version 4.4.23(1)-release (aarch64-suse-linux-gnu)

function main() {
  # Output our time in ISO8601 with UTC time 
  ISO_8601_DATE_TIME=$(date -u +%F"T"%R:%S)
  # Our requisite files
  CURRENT_USERS=/var/log/current_users
  USER_CHANGES=/var/log/user_changes
  # Read through the input from the first script and compute an MD5 sum
  USERS_HASH=$(md5sum "${@}" | awk '{print $1}')
  # Does /var/log/current_users exist?
  if [[ ! -e "${CURRENT_USERS}" ]]; then
    # Output the user hash into /var/log/current_users,
    # as this is the first time this script has run.
    echo "${USERS_HASH}" >> "${CURRENT_USERS}"
  else
    CURRENT_USER_HASH=$(cat "${CURRENT_USERS}")
    if [[ "${CURRENT_USER_HASH}" != "${USERS_HASH}" ]]; then
    # The users data has changed and must be updated in /var/log/user_changes
      echo "${ISO_8601_DATE_TIME} changes occurred" >> "${USER_CHANGES}"
      # Replace the old hash in /var/log/current_users
      echo "${USERS_HASH}" > "${CURRENT_USERS}"
    fi
  fi
}

main "${@}"
