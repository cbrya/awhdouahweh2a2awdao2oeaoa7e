variable "region" {
  default = "us-west-2"
}

variable "vpc_cidr_block" {
  default = "10.17.0.0/16"
}
